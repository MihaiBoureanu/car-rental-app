import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent implements OnInit {
  @Input('toolbarType') toolbarType: string = 'home';
  @Input('title') title: string = 'welcome';

  constructor(private router: Router) {}

  ngOnInit(): void {}

  onAdmin(): void {
    this.router.navigate(['/', 'admin'], { replaceUrl: true });
  }

  onHome(): void {
    this.router.navigate(['/', 'home'], { replaceUrl: true });
  }

  onUsers(): void {
    this.router.navigate(['/', 'users'], { replaceUrl: true });
  }

  onItems(): void {
    this.router.navigate(['/', 'items'], { replaceUrl: true });
  }
}
