import { ItemsService } from './../items/items.service';
import { UsersService } from './../users/users.service';
import { Component, OnInit } from '@angular/core';
import { ItemModel } from '../items/models/item.model';
import { UserModel } from '../users/models/user.model';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ApiResponse } from '../utils/api-response.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})
export class AdminComponent implements OnInit {
  registerForm: FormGroup;
  contentForm: FormGroup;

  users: Array<UserModel> = [];
  items: Array<ItemModel> = [];

  constructor(
    private userService: UsersService,
    private itemsService: ItemsService,
    private formBuilder: FormBuilder
  ) {
    this.registerForm = this.formBuilder.group({});
    this.contentForm = this.formBuilder.group({});

    this.setupRegisterForm();
    this.setupContentForm();
  }

  ngOnInit(): void {
    // this.users = this.userService.loadUsers();
    this.userService.getUsers().subscribe((response) => {
      this.users = (response as ApiResponse).result
    });
    // this.items = this.itemsService.loadItems();3
    this.itemsService.getItems().subscribe((response) => {
      console.log(response);
      this.items = (response as ApiResponse).result;
    });
  }

  getErrorMessage(controlName: string) {
    switch (controlName) {
      case 'email': {
        if (this.registerForm.controls['email'].hasError('required')) {
          return 'You must enter a value';
        }

        if (this.registerForm.controls['email'].hasError('email')) {
          return 'Not a valid email';
        }

        return '';
      }

      case 'password': {
        if (this.registerForm.controls['password'].hasError('required')) {
          return 'Not a valid password';
        }

        return '';
      }

      case 'firstName': {
        if (this.registerForm.controls['firstName'].hasError('required')) {
          return 'Not a valid firstName';
        }

        return '';
      }

      case 'lastName': {
        if (this.registerForm.controls['lastName'].hasError('required')) {
          return 'Not a valid lastName';
        }

        return '';
      }

      case 'phone': {
        if (this.registerForm.controls['phone'].hasError('required')) {
          return 'Not a valid phone';
        }

        return '';
      }

      case 'username': {
        if (this.registerForm.controls['username'].hasError('required')) {
          return 'Not a valid username';
        }

        return '';
      }

      case 'repassword': {
        if (this.registerForm.controls['repassword'].hasError('required')) {
          return 'Not a valid password';
        }

        var password = this.registerForm.controls['password'].value;
        var repassword = this.registerForm.controls['repassword'].value;

        if (password != repassword) {
          return 'Passwords not match!';
        }

        return '';
      }
      case 'title': {
        if (this.contentForm.controls['title'].hasError('title')) {
          return 'Not valid field';
        }

        return '';
      }
      case 'description': {
        if (this.contentForm.controls['description'].hasError('description')) {
          return 'Not valid field';
        }

        return '';
      }
      case 'itemType': {
        if (this.contentForm.controls['itemType'].hasError('itemType')) {
          return 'Not valid field';
        }

        return '';
      }
      case 'quantity': {
        if (this.contentForm.controls['quantity'].hasError('quantity')) {
          return 'Not valid field';
        }

        return '';
      }
      case 'imageURL': {
        if (this.contentForm.controls['imageURL'].hasError('imageURL')) {
          return 'Not valid field';
        }

        return '';
      }
      case 'price': {
        if (this.contentForm.controls['price'].hasError('price')) {
          return 'Not valid field';
        }

        return '';
      }
      default:
        return '';
    }
  }

  onRegister(): void {
    if(this.registerForm.value){
      this.userService.addUser(this.registerForm.value)
      .subscribe((response) => {
        this.setupRegisterForm();
        this.userService.getUsers().subscribe((response) => {
          this.users = (response as ApiResponse).result
        });
      });
    }    
  }

  onAddContent(): void {
    if(this.contentForm.valid){
      this.itemsService
      .createItem(this.contentForm.value)
      .subscribe((response) => {
        this.setupContentForm();
        this.itemsService.getItems().subscribe((response) => {
          console.log(response);
          this.items = (response as ApiResponse).result;
        });
      });
    }
  }

  private setupContentForm(): void {
    this.contentForm = this.formBuilder.group({
      id: new FormControl(null),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      itemType: new FormControl('', [Validators.required]),
      quantity: new FormControl('', [Validators.required, Validators.email]),
      imageURL: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
    });
  }

  private setupRegisterForm(): void {
    this.registerForm = this.formBuilder.group({
      id: new FormControl(null),
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required]),
      repassword: new FormControl('', [Validators.required]),
    });
  }
}
