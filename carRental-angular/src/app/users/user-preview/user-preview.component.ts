import { Component, Input, OnInit } from '@angular/core';
import { UserModel } from '../models/user.model';

@Component({
  selector: 'app-user-preview',
  templateUrl: './user-preview.component.html',
  styleUrls: ['./user-preview.component.css'],
})
export class UserPreviewComponent implements OnInit {
  @Input('user') user: UserModel;
  @Input('isAdminPage') isAdminPage: boolean = false;

  constructor() {
    this.user = {
      id: '',
      firstName: '',
      lastName: '',
      username: '',
      password: '',
      email: '',
      phone: '',
      items: [],
    };
  }

  ngOnInit(): void {}

  onCreate(): void {}

  onDelete(): void {}
}
