import { UsersService } from './users.service';
import { Component, OnInit } from '@angular/core';
import { UserModel } from './models/user.model';
import { ApiResponse } from '../utils/api-response.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: Array<UserModel> = [];

  constructor(private userService: UsersService) {}

  ngOnInit(): void {
    this.userService.getUsers().subscribe((response) => {
      this.users = (response as ApiResponse).result
    });
  }
}
