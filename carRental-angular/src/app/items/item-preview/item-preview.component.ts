import { Component, Input, OnInit } from '@angular/core';
import { ItemModel } from '../models/item.model';

@Component({
  selector: 'app-item-preview',
  templateUrl: './item-preview.component.html',
  styleUrls: ['./item-preview.component.css'],
})
export class ItemPreviewComponent implements OnInit {
  @Input('item') item: ItemModel;

  constructor() {
    this.item = {
      id: '0',
      title: `Title`,
      description: `Description`,
      imageURL: 'https://image.shutterstock.com/z/stock-vector-rent-car-logo-design-vector-1072548182.jpg',
      itemType: 'car',
      price: '100',
      quantity: '0',
    };
  }

  ngOnInit(): void {}

  getItemTypeImage() {
    switch (this.item.itemType) {
      case 'car': {
        return 'http://www.freepngclipart.com/download/car/76383-group-auto-car-detailing-dealership-mp-vehicle.png';
      }
      case 'motorcycle': {
        return 'https://image.freepik.com/free-vector/motorcycle-vector-logo_20448-45.jpg';
      }

      default: {
        return 'https://image.shutterstock.com/z/stock-vector-rent-car-logo-design-vector-1072548182.jpg';
      }
    }
  }
}
