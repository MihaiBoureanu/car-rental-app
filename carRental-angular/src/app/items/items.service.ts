import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { UsersService } from './../users/users.service';
import { Injectable } from '@angular/core';
import { ItemModel } from './models/item.model';

@Injectable({
  providedIn: 'root',
})
export class ItemsService {
  constructor(
    private userService: UsersService,
    private httpClient: HttpClient
  ) {}

  loadItems(): Array<ItemModel> {
    var items = [];

    for (let i = 0; i < 10; i++) {
      items.push({
        id: `${i}`,
        title: `Title ${i}`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
        imageURL: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
        itemType: 'car',
        price: '100',
        quantity: '0',
        user: this.userService.getUser(),
      });
    }

    return items;
  }

  public getItems() {
    return this.httpClient.get(`${environment.apiUrl}/items`, {
      headers: this.userService.getHeaders(),
    });
  }

  public createItem(itemForm: any) {
    return this.httpClient.post(`${environment.apiUrl}/items`, itemForm, {
      headers: this.userService.getHeaders(),
    });
  }

  public updateItem(itemForm: any) {
    return this.httpClient.put(
      `${environment.apiUrl}/items/${itemForm.id}`,
      itemForm,
      { headers: this.userService.getHeaders() }
    );
  }

  public deleteItem(itemForm: any) {
    return this.httpClient.delete(
      `${environment.apiUrl}/items/${itemForm.id}`,
      { headers: this.userService.getHeaders() }
    );
  }
}
