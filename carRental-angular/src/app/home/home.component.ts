import { ItemsService } from './../items/items.service';
import { UsersService } from './../users/users.service';
import { Component, OnInit } from '@angular/core';
import { ItemModel } from '../items/models/item.model';
import { ApiResponse } from '../utils/api-response.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  homePageInfo: HomePageInfo;
  items: Array<ItemModel> = [];

  constructor(
    private userService: UsersService,
    private itemsService: ItemsService
  ) {
    this.homePageInfo = {
      title: 'Mihai\'s Rent a Car',
      description1:
        'One of our top priorities is to adjust each package we offer to our customer’s exact needs. We offer a variety of options that can enhance your experience, always according to your necessities,' +
        ' and help you get the best out of your holidays or your business trip.',
      description2: '*COVID-19 UPDATE FOR NON-EU CITIZEN TRAVELING TO EU*\n' +
        '- Currently, non-EU citizens are allowed to travel to the European Union but are required to self-isolate for two weeks upon arrival.',
      author: 'Boureanu Mihai',
    };
  }

  ngOnInit(): void {
    this.itemsService.getItems().subscribe((response) => {
      console.log(response);
      this.items = (response as ApiResponse).result;
    });
  }
}

export interface HomePageInfo {
  title: string;
  description1: string;
  description2: string;
  author: string;
}
