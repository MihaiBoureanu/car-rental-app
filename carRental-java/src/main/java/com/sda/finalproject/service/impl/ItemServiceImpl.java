package com.sda.finalproject.service.impl;

import com.sda.finalproject.dao.ItemDao;
import com.sda.finalproject.model.Item;
import com.sda.finalproject.model.ItemBody;
import com.sda.finalproject.model.User;
import com.sda.finalproject.service.ItemService;
import com.sda.finalproject.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value="itemService")
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private UserService userService;

    @Override
    public Item save(ItemBody item) {
        Item newItem = new Item();
        newItem.setTitle(item.getTitle());
        newItem.setItemType(item.getItemType());
        newItem.setDescription(item.getDescription());
        newItem.setPrice(item.getPrice());
        newItem.setQuantity(item.getQuantity());
        newItem.setImageURL(item.getImageURL());

        User user = userService.findOne(item.getUser());

        newItem.setUser(user);

        return itemDao.save(newItem);
    }

    @Override
    public List<Item> findAll() {
        List<Item> items = new ArrayList<>();

        itemDao.findAll().iterator().forEachRemaining(items::add);

        return items;
    }

    @Override
    public void delete(int id) {
        itemDao.deleteById(id);
    }

    @Override
    public Item findOne(String title) {
        return itemDao.findByTitle(title);
    }

    @Override
    public Item findById(int id) {
        Optional<Item> optionalItem = itemDao.findById(id);
        return optionalItem.isPresent() ? optionalItem.get() : null;
    }

    @Override
    public ItemBody update(ItemBody itemBody) {
        Item item = findById(itemBody.getId());
        if(item != null) {
            BeanUtils.copyProperties(itemBody, item);
            itemDao.save(item);
        }
        return itemBody;
    }
}
