package com.sda.finalproject.dao;

import com.sda.finalproject.model.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemDao extends CrudRepository<Item, Integer> {
    Item findByTitle(String title);
}
