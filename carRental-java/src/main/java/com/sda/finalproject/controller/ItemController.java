package com.sda.finalproject.controller;

import com.sda.finalproject.model.*;
import com.sda.finalproject.service.ItemService;
import com.sda.finalproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/items")
public class ItemController {
    @Autowired
    private ItemService itemService;

    @PostMapping
    public ApiResponse<Item> saveItem(@RequestBody ItemBody item){
        return new ApiResponse<>(HttpStatus.OK.value(), "Item saved successfully.",itemService.save(item));
    }

    @GetMapping
    public ApiResponse<List<Item>> listItem(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Item list fetched successfully.",itemService.findAll());
    }

    @GetMapping("/{id}")
    public ApiResponse<Item> getOne(@PathVariable int id){
        return new ApiResponse<>(HttpStatus.OK.value(), "Item fetched successfully.",itemService.findById(id));
    }

    @PutMapping("/{id}")
    public ApiResponse<ItemBody> update(@RequestBody ItemBody itemBody) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Item updated successfully.",itemService.update(itemBody));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        itemService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Item deleted successfully.", null);
    }
}
